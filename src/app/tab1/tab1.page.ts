import {Component, OnInit} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {AlertController, LoadingController, ModalController, NavParams} from '@ionic/angular';
import { ProductosServiceService} from '../services/productos-service.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  /// uid de la categoria
  uidCategoria: any;
  descripcion: any;
  nombre: any;
  precio: any;
  seleccion: any;
  showCat: any;
  uidProducto: any;
  //// arreglos
  listaProductos: any = [];
  listaCategorias: any = [];
  listaCarrito: any = [];

  constructor(private fs: AngularFirestore,
              public alertController: AlertController,
              public modalCtrl: ModalController,
              private router: Router,
              public loadingCtrl: LoadingController,
              public serviceProd: ProductosServiceService) {
    this.seleccion = 'all';

  }

  ngOnInit() {
    this.getProducts();
    this.getCategories();
  }
  async agregarProductoCarrito(datos) {
    const modal = await this.modalCtrl.create({
      component: PopinAgregarProductoCarrito,
      componentProps: datos,
      cssClass: 'modal-halfscreen'
    });
    await modal.present();
    const data = await modal.onWillDismiss();
    this.listaCarrito.push(data);
    const dataJson = JSON.stringify(this.listaCarrito);
    localStorage.setItem('playlisttest', dataJson);
    console.log(dataJson);
  }

  async editarProducto(datos) {
    const modal = await this.modalCtrl.create({
      component: PopinEditarProducto,
      componentProps: datos,
      cssClass: 'modal-halfscreen'
    });
    await modal.present();
    this.ngOnInit();
  }

  getCategories() {
    this.serviceProd.getCategorias().then(
        async (results) => {
          this.listaCategorias =  results;
        }, (err) => console.log(err)
    );
  }

  getCategory() {
    console.log('cat: ', this.seleccion);
    this.serviceProd.getCategory(this.seleccion).then(
        async (results) => {
          this.listaProductos =  results;
          console.log(this.listaProductos);
        }, (err) => console.log(err)
    );
  }
  getProducts() {
    this.serviceProd.getProductos().then(
        async (results) => {
          this.listaProductos =  results;
          console.log(this.listaProductos);
          /*this.nombre = results[0].data.nombre;
          console.log('nombre: ' + this.nombre);
          this.descripcion = results[0].data.descripcion;
          console.log('desc: ' + this.descripcion);
          this.uidProducto = results[0].uid;
          console.log('tarj', this.uidProducto);
          this.uidCategoria = results[0].data;*/
          // this.url = 'https://www.grupossc.com/grupossc.com/propietarioftp/animacion.php?nombre=' + this.nombre + '&&descripcion='
          //     + this.descripcion + '&&logo=' + uid;
        }, (err) => console.log(err)
    );
  }

}

@Component({
  selector: 'app-tab1',
  templateUrl: 'popin-editar-producto.html',
  styleUrls: ['tab1.page.scss']
})
export class PopinEditarProducto {
  categorias: any = [];
  usuario: any;
  public respuestaImagenEnviada;
  public resultadoCarga;

  /**
   * Variable to know if data is processing
   * @type {HTMLIonLoadingElement}
   */
  public loading: HTMLIonLoadingElement;
  datos = {
    id: '',
    nombre: '',
    precio: '',
    descripcion: ''
  };
  constructor(
      public modalCtrl: ModalController,
      public navParams: NavParams,
      public alertController: AlertController,
      public loadingCtrl: LoadingController,
      public  http: HttpClient,
      public serviceProd: ProductosServiceService
  ) {
    this.datos.id = this.navParams.get('id');
    this.datos.nombre = this.navParams.get('nombre');
    this.datos.descripcion = this.navParams.get('descripcion');
    this.datos.precio = this.navParams.get('precio');
    // this.datos.nombre = this.navParams.get('nombre');
    console.log('id: ', this.datos.id);
  }
  dismiss() {
    this.modalCtrl.dismiss();

  }

  updateName(info) {
    console.log('nvo nombre', info);
    const data = {
      id: this.datos.id,
      nombre: info
    };
    this.serviceProd.editNameProd(data).then(res => {
      console.log('respuesta: ', res);
    });
  }

  updatePrecio(info) {
    console.log('nvo precio', info);
    const data = {
      id: this.datos.id,
      precio: info
    };
    this.serviceProd.editPrecioProd(data).then(res => {
      console.log('respuesta: ', res);
    });
  }

  updateDescripcion(info) {
    console.log('nvo Desc', info);
    const data = {
      id: this.datos.id,
      descripcion: info
    };
    this.serviceProd.editDescripcionProd(data).then(res => {
      console.log('respuesta: ', res);
    });
  }
}


@Component({
  selector: 'app-tab1',
  templateUrl: 'agregar-carrito.html',
  styleUrls: ['tab1.page.scss']
})
export class PopinAgregarProductoCarrito {
  categorias: any = [];
  usuario: any;
  public respuestaImagenEnviada;
  public resultadoCarga;
  toppings: any[];

  /**
   * Variable to know if data is processing
   * @type {HTMLIonLoadingElement}
   */
  public loading: HTMLIonLoadingElement;
  datos = {
    id: '',
    nombre: '',
    precio: 0,
    descripcion: '',
    subtotal: ''
  };
  carrito = {
    id_prod: '',
    nombre: '',
    cantidad: 0,
    subtotal: 0,
    total: 0
  }
  totalCarrito: any;
  extra: any;
  carritoProductos: any = [];
  carritoProductosMasExtras: any = [];
  constructor(
      public modalCtrl: ModalController,
      public navParams: NavParams,
      public alertController: AlertController,
      public loadingCtrl: LoadingController,
      public  http: HttpClient,
      public serviceProd: ProductosServiceService
  ) {
    this.datos.id = this.navParams.get('id');
    this.datos.nombre = this.navParams.get('nombre');
    this.datos.descripcion = this.navParams.get('descripcion');
    this.datos.precio = this.navParams.get('precio');
    // this.datos.nombre = this.navParams.get('nombre');
    console.log('id: ', this.datos.id);
    this.extra = false;
  }
  dismiss() {
    this.modalCtrl.dismiss();
  }
  agregarCarrito() {
    this.extra = true;
    this.carrito.id_prod = this.datos.id;
    this.carrito.nombre = this.datos.nombre;
    this.carrito.subtotal = this.carrito.cantidad * this.datos.precio;
    this.carritoTotal(this.carrito.subtotal);
  }
  carritoTotal(sub) {
    this.totalCarrito = this.carrito.total + sub;
    localStorage.setItem('total', this.totalCarrito);
    this.carritoProductos.push(this.carrito);
    this.carrito = {
      id_prod: '',
      nombre: '',
      cantidad: 0,
      subtotal: 0,
      total: 0};
  }
  carritoAgregarExtras() {
    this.carritoProductos.push(this.toppings);
    this.carritoProductosMasExtras.push(this.carritoProductos);
    /// console.log(this.carritoProductosMasExtras);
    this.modalCtrl.dismiss(this.carritoProductosMasExtras);
  }

  selectExtra() {
    console.log(this.toppings);
  }

}


