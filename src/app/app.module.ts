import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AngularFireModule} from '@angular/fire';
import { AngularFireAuthModule} from '@angular/fire/auth';
import { AngularFireFunctionsModule} from '@angular/fire/functions';
import { AngularFireDatabaseModule} from '@angular/fire/database';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirestoreModule} from '@angular/fire/firestore';
import { ProductosServiceService } from './services/productos-service.service';

const config = {
  apiKey: 'AIzaSyBnJuxpl1L9Ag2REMeHk5aZ79WG6JJKyCg',
  authDomain: 'prueba-tecnica-f34f4.firebaseapp.com',
  databaseURL: 'https://prueba-tecnica-f34f4.firebaseio.com',
  projectId: 'prueba-tecnica-f34f4',
  storageBucket: 'prueba-tecnica-f34f4.appspot.com',
  messagingSenderId: '901884517425',
  appId: '1:901884517425:web:c84b508e613e076e4ec910',
  measurementId: 'G-17SSDSBEWG'
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireFunctionsModule,
    AngularFireStorageModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(config),
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    StatusBar,
    ProductosServiceService,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
