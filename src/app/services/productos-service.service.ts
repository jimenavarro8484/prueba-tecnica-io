import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
// You don't need to import firebase/app either since it's being imported above
import 'firebase/auth';
import 'firebase/firestore';

@Injectable({
  providedIn: 'root'
})
export class ProductosServiceService {

  constructor() { }

    editDescripcionProd(data) {
        return new Promise( function(resolve) {
            const updateDesc = firebase
                .firestore()
                .doc(`/Productos/${data.id}`);
            return updateDesc.update(data).then(function () {
                console.log('Name successfully updated!');
            });
            resolve(' Description updated');
        });
    }
  // Función para editar nombre del producto
  editNameProd(data) {
      return new Promise( function(resolve) {
          const updateName = firebase
              .firestore()
              .doc(`/Productos/${data.id}`);
          return updateName.update(data).then(function () {
              console.log('Name successfully updated!');
          });
          resolve('Name updateed');
      });
  }

    editPrecioProd(data) {
        return new Promise( function(resolve) {
            const updatePrecio = firebase
                .firestore()
                .doc(`/Productos/${data.id}`);
            return updatePrecio.update(data).then(function () {
                console.log('Name successfully updated!');
            });
            resolve('Precio updated');
        });
    }
  getCategory(cat) {
      const listProductosByCat = [];
      // tslint:disable-next-line:only-arrow-functions
      return new Promise( function(resolve) {
          const getProdByCat = firebase
              .firestore()
              .collection(`/Productos/`).where('categoria', '==', cat);
          getProdByCat.get().then((snapshot) => {
              snapshot.forEach((doc) => {
                  // console.log(doc.data());
                  const data = {
                      id: doc.id,
                      nombre: doc.data().nombre,
                      idCat: doc.data().categoria,
                      descripcion: doc.data().descripcion,
                      precio: doc.data().precio
                  };
                  listProductosByCat.push(data);
              });
              resolve(listProductosByCat);
          }).catch(err => {
              console.log('Error getting documents', err);
              resolve('error');
          });
      });
  }

    getProductos() {
        const listProductos = [];
        // tslint:disable-next-line:only-arrow-functions
        return new Promise( function(resolve) {
            const getProd = firebase
                .firestore()
                .collection(`/Productos`);
            getProd.get().then((snapshot) => {
                snapshot.forEach((doc) => {
                    // console.log(doc.data());
                    const data = {
                        id: doc.id,
                        nombre: doc.data().nombre,
                        idCat: doc.data().categoria,
                        descripcion: doc.data().descripcion,
                        imagen: doc.data().imagen,
                        precio: doc.data().precio
                    };
                    listProductos.push(data);
                });
                resolve(listProductos);
            }).catch(err => {
                console.log('Error getting documents', err);
                resolve('error');
            });
        });
    }

    getCategorias() {
        const listaCategorias = [];
        // tslint:disable-next-line:only-arrow-functions
        return new Promise( function(resolve) {
            const getCat = firebase
                .firestore()
                .collection(`/Categorias`);
            getCat.get().then((snapshot) => {
                snapshot.forEach((doc) => {
                    /// console.log(doc.data());
                    const data = {
                        id: doc.id,
                        nombre: doc.data().nombre,
                    };
                    listaCategorias.push(data);
                });
                resolve(listaCategorias);
            }).catch(err => {
                console.log('Error getting documents', err);
                resolve('error');
            });
        });
    }
}
